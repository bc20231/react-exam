useEffect is a React Hook that can synchronize a component with a non-React component.
For example, we want to track the number of times clicking a button, and want to show
	the counts in both <p> in the function component and document.title.
If useEffect is no used, when you click on the button and the count increases by one,
	only the count in <p> in the function component can be updated,
	because document.title is not a React component, so useEffect is required here.
In general, it is used for performing side effects in components after every render.
Some of its uses include fetching data and updating DOM directly.