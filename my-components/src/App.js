import './App.css';
import React, { useState } from 'react';

// Hello Component
function Hello() {
	const [text, setText] = useState('');

	return (
		<div className="hello">
			<p>Enter your name: {text}</p>
			<input type="text" onChange={(e) => setText(e.target.value)} />
			<button type="button">Submit</button> 
		</div>
	);
}

// Cars Component
function Cars({ cars }) {
	const carList = cars.map((car, index) => (
		<li key={index}>I am a {car}</li>
	));
	
	return (
		<div className="cars">
			<h1>Who lives in my garage?</h1>
			<ul>{carList}</ul>
		</div>
	);
}

function App() {
	const cars = ['Ford', 'BMW', 'Audi'];
	
	return (
		<div>
			<Hello />
			<Cars cars={cars} />
		</div>
	);
}

export default App;
